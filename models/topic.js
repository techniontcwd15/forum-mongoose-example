const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CommentSchema = new Schema({
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
    required: true
  },
  body: String
});

const TopicSchema = new Schema({
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user',
    required: true
  },
  title: {
    type: String,
    required: true
  },
  body: String,
  timestamp: Date,
  comments: [CommentSchema]
});

module.exports = mongoose.model('topic', TopicSchema);
