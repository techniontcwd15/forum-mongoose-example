const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('express-session');

const Topic = require('./models/topic');
const User = require('./models/user');

mongoose.connect('mongodb://localhost/forum');

const app = express();

app.use(bodyParser.json());
app.use(session({
  secret: 'telaviv'
}));

/**
 * add topic to the mongodb
 */
app.post('/topics', async (req, res) => {
  if(req.session.user && !req.session.user._id) {
    return res.sendStatus(304);
  }

  try {
    await Topic.create({...req.body, author: req.session.user._id});
    res.sendStatus(204);
  } catch(err) {
    res.send(500, err.message);
  }
});

app.get('/user/:id/topics', async (req, res) => {
  const topics = await Topic.find({author: req.params.id})
    .populate('author');

  res.json(topics);
});

/**
 * add comment to topic
 */
app.post('/topics/:id/comments', async (req, res) => {
  try {
    const topic = await Topic.findById(req.params.id);
    topic.comments.push(req.body);
    await topic.save();

    res.sendStatus(204);
  } catch(err) {
    res.send(500, err.message);
  }
});

/**
 * add comment to topic
 */
app.get('/topics/:id/comments', async (req, res) => {
  const topic = await Topic.findById(req.params.id);

  res.json(topic.comments);
});

/**
 * get all the topics from the mongodb
 */
app.get('/topics', async (req, res) => {
  res.json(await Topic.find().populate('author'));
});

app.post('/users', async (req, res) => {
  try {
    await User.create(req.body);
    res.sendStatus(204);
  } catch(err) {
    res.send(500, err.message);
  }
});

app.post('/users/login', async (req, res) => {
  const user = await User.findOne({name: req.body.name});

  if(user.password !== req.body.password) {
    return res.sendStatus(304);
  }

  req.session.user = user.toObject();
  res.sendStatus(204);
});

app.listen(8000, () => {
  console.log('server listen on 8000');
});
