const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/testDB');

const Schema = mongoose.Schema;

const PostSchema = new Schema({
  author: String,
  title: {
    type: String,
    required: true
  },
  body: String,
  timestamp: Date,
  comments: [{author: String, body: String}]
});

const Post = mongoose.model('post', PostSchema);

(async () => {
  // const post = new Post();
  // post.title = 'sss';
  // post.body = '';
  // for (var i = 0; i < 100; i++) {
  //   post.body += i;
  // }
  // post.save();

  // await Post.create({
  //   author: 'a',
  //   title: 'b',
  //   body: 'c'
  // });

  // const post = await Post.findById('5a6f455eede1c17c083fac8f');
  // post.name = 'd';
  // await post.save();
  // post.comments.push({author: 'q', body:'w'});
  // await post.save();

  mongoose.disconnect();
})();
